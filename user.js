const MongoClient = require('mongodb').MongoClient;
const url = "mongodb://localhost:27017/";
const dbname = "demo"
const ObjectID = require('mongodb').ObjectID

module.exports = {
	loginUser: async (username, password) => {
		client = await MongoClient.connect(url, { useNewUrlParser: true });
		db = client.db(dbname);
		var query = { username:username, password:password };
		var result = await db.collection("users").find(query).toArray();

		if (result.length == 0) {
			return {err:"wrong username/password"}
			client.close();
		}else{
			var last_login_date = new Date(result[0].last_login)
			var current_date = new Date(Date.now())
			var bonus_points = parseInt(parseInt((current_date - last_login_date) / (1000 * 60 * 60))/3)*50;
			bonus_points = bonus_points + result[0].points_free
			if (bonus_points > 200){
				bonus_points = 200
			}
			result[0].points_free = bonus_points
			var ObjectID = require('mongodb').ObjectID;
			var query = { username : username };
			var newvalues = {$set: {last_login: Date(Date.now()), points_free:bonus_points} };
			db.collection("users").updateOne(query, newvalues)

			await module.exports.updateUserPoints(username, bonus_points, 0)
			var query = {
				user:username,
				type: "Cr",
				free_points:bonus_points,
				purchased_points:0,
				description:"Bonus points",
				transaction_date: Date(Date.now())
			};
			db.collection("transactionHistory").insertOne(query)

			return result
		}
	},

	signUpUser: async (username, password) => {
		existing = await module.exports.getUserByUserName(username)
		if (existing.length > 0) {
			return {err:"username already exists"}
		}else{
			var objectId = new ObjectID();
			client = await MongoClient.connect(url, { useNewUrlParser: true });
			db = client.db(dbname);
			var query = {
				username,
				password,
				points_free:0,
				points_purchased:0,
				last_login: Date(Date.now())
			};
			db.collection("users").insertOne(query)
			return true
		}
	},

	getUserByUserName: async (username) => {
	 	client = await MongoClient.connect(url, { useNewUrlParser: true });
		db = client.db(dbname);
		var query = { username:username };
		result = await db.collection("users").find(query).toArray();
		return result
	},

	fetchItems: async () => {
		client = await MongoClient.connect(url, { useNewUrlParser: true });
		db = client.db(dbname);
		result = await db.collection("products").find().toArray();
		client.close();
		return result
	},

	fetchItemById: async (itemId) => {
		client = await MongoClient.connect(url, { useNewUrlParser: true });
		db = client.db(dbname);
		var ObjectID = require('mongodb').ObjectID;
		var query = { _id:new ObjectID(itemId) };
		result = await db.collection("products").find(query).toArray();
		client.close();
		if (result.length == 0) {
			return {err:"wrong item id"}
		}else{
			return result[0]
		}
	},

	addItem: async (id, username, free_points, purchased_points) => {
		var item = await module.exports.fetchItemById(id)
		if (item.hasOwnProperty("err")) {
			res.redirect('/purchaseItem?err='+item['err'])
		}else{
			client = await MongoClient.connect(url, { useNewUrlParser: true });
			db = client.db(dbname);
			var query = {
				username:username,
				itemId:id,
				price:item.price,
				date_purchased: Date(Date.now())
			};
			db.collection("purchasedItems").insertOne(query)

			var transaction_free_points = 0
			var transaction_purchased_points = 0

			if (free_points < item.price){
				transaction_free_points = free_points
				transaction_purchased_points = purchased_points - (item.price-free_points)
				purchased_points = purchased_points - (item.price-free_points)
				free_points = 0
			}else if(free_points >= item.price) {
				transaction_free_points = item.price
				transaction_purchased_points = 0
				free_points = free_points - item.price
			}
			module.exports.updateUserPoints(username, free_points, purchased_points)
			var query = {
				user:username,
				type: "Dr",
				free_points:transaction_free_points,
				purchased_points:transaction_purchased_points,
				description:"Points purchased",
				transaction_date: Date(Date.now())
			};
			db.collection("transactionHistory").insertOne(query)
			return {free_points:free_points, purchased_points:purchased_points}
		}
	},

	updateUserPoints: async (username, free_points, purchased_points) => {
		client = await MongoClient.connect(url, { useNewUrlParser: true });
		db = client.db(dbname);
		var myquery = { username: username };
		var newvalues = {$set: {points_free: free_points, points_purchased:purchased_points} };
		db.collection("users").updateOne(myquery, newvalues)
	},

	getPurchasedItems: async (username) => {
		client = await MongoClient.connect(url, { useNewUrlParser: true });
		db = client.db(dbname);
		var query = { username:username };
		var result = await db.collection("purchasedItems").find(query).toArray();
		var response = new Set()
		for (index in result) {
			let item_details = new Set()
			let product_details = await module.exports.fetchItemById(result[index]["itemId"])
			item_details.add({
				product:result[index],
				product_details,
			})
			response.add(...item_details)
		}
		client.close();
		return [...response]
	},

	purchasePoints: async(username, purchased_points, existing_points) => {
		client = await MongoClient.connect(url, { useNewUrlParser: true });
		db = client.db(dbname);
		var total_points = parseInt(purchased_points)+parseInt(existing_points)
		var query = { username: username };
		var newvalues = {$set: { points_purchased: total_points } };
		db.collection("users").updateOne(query, newvalues)
		var query = {
			user:username,
			type: "Cr",
			free_points:0,
			purchased_points:purchased_points,
			description:"Points purchased",
			transaction_date: Date(Date.now())
		};
		db.collection("transactionHistory").insertOne(query)
		return total_points
	},

	getTrasactionHistory: async (username) => {
		client = await MongoClient.connect(url, { useNewUrlParser: true });
		db = client.db(dbname);
		var query = { user:username };
		var result = await db.collection("transactionHistory").find(query).toArray();
		return result
	}
}