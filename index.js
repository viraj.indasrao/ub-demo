const port = 3000
const user = require('./user')
const express = require('express')
const app = express()
const session = require('express-session');
app.use(session({secret: 'my-secret', cookie: { maxAge: 600000 }}));
var sess;

app.set('view engine', 'pug')
var bodyParser = require('body-parser')
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

app.get('/', (req, res) => {
	sess = req.session;
	data = {}
	let err = req.query.err;
    if (err) {
        data["err"] = err
    }
	res.render('login', data)
})

app.post('/login', async (req, res) => {
	sess = req.session;
	let response = await user.loginUser(req.body.username, req.body.password, sess)
	if (response.hasOwnProperty("err")) {
		res.redirect('/?err='+response['err'])
	}else{
		sess.username = response[0].username
		sess.freePoints = response[0].points_free
		sess.purchasedPoints = response[0].points_purchased
		res.redirect('/getPoints')
	}
})

app.get('/getPoints', async (req, res) => {
	sess = req.session;
	if (sess.username) {
		data = {
			free_points:sess.freePoints,
			purchased_points:sess.purchasedPoints
		}
		res.send(data)
	} else {
		res.redirect('/')
	}
})

app.get('/signup', (req, res) => {
	data = {}
	let err = req.query.err;
    if (err) {
        data["err"] = err
    }
	res.render('signup', data)
})

app.get('/getItems', async (req, res) => {
	sess = req.session;
	if (sess.username) {
		let items = await user.getPurchasedItems(sess.username)
		data = {items}
		res.render('getItems', data)
	}
})

app.get('/purchasePoints', async (req, res) => {
	sess = req.session;
	if (sess.username) {
		let points = req.query.points ? req.query.points : null
		if (points != null) {
			console.log("sess.purchasedPoints >> ", sess.purchasedPoints)
			total_points = await user.purchasePoints(sess.username, points, sess.purchasedPoints)
			console.log("total_points >> ", total_points)
			sess.purchasedPoints = total_points
		}
		var trasactionHistory = await user.getTrasactionHistory(sess.username)
		var data = {trasactionHistory: trasactionHistory}
		console.log(data)
		res.render('purchasePoints', data)
	}
})

app.get('/purchaseItem', async (req, res) => {
	sess = req.session;
	if (sess.username) {
		let id = req.query.id ? req.query.id : null
		if (id != null) {
			var data = await user.addItem(id, sess.username, sess.freePoints, sess.purchasedPoints)
			sess.freePoints = data["free_points"]
			sess.purchasedPoints = data["purchased_points"]
		}
		items = await user.fetchItems()
		var points = sess.freePoints+sess.purchasedPoints
		data = {
			items,
			points
		}
		res.render('purchaseItem', data)
	} else {
		res.redirect('/')
	}
})

app.post('/signup-process', async (req, res) => {
	let response = await user.signUpUser(req.body.username, req.body.password)
	if (response.hasOwnProperty("err")) {
		res.redirect('/signup?err='+response['err'])
	}else{
		res.redirect('/')
	}
})

app.listen(port, () => console.log(`App listening on port ${port}!`))
